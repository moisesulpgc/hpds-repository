package expresionevaluator;

import org.junit.Assert;
import org.junit.Test;

public class ExpressionEvaluatorTest {

    public ExpressionEvaluatorTest() {
    }

    @Test
    public void testConstant() {
        Assert.assertEquals(1, new Constant(1).calculate());
        Assert.assertEquals(4, new Constant(4).calculate());
    }
    @Test
    public void testOperacion() {
        Assert.assertEquals(3, new Constant("+", new Constant(1), new Constant(2)).calculate());
        Assert.assertEquals(2, new Constant("-", new Constant(4), new Constant(2)).calculate());
        Assert.assertEquals(8, new Constant("*", new Constant(4), new Constant(2)).calculate());
        Assert.assertEquals(2, new Constant("/", new Constant(4), new Constant(2)).calculate());
    }
    
    public interface  Expression{
        public int calculate();
    }
    
    public class Constant implements Expression{
        private int value;
        private String operator;
        private Constant left;
        private Constant right;
        
        public Constant(String operator, Constant left, Constant right) {  
            this.operator = operator;
            this.left = left;
            this.right = right;
        }

        public Constant(int value) {
            this.value = value;
            this.operator = null;
        }
        

        public int calculate(){
            if (operator == null)
                return value;
            if (operator.equals("+"))
                return left.calculate() + right.calculate();
            if (operator.equals("-"))
                return left.calculate() - right.calculate();
            if (operator.equals("*"))
                return left.calculate() * right.calculate();
            if (operator.equals("/"))
                return left.calculate() / right.calculate();
            return 0;
        }
    }
    
    
    
}
